const { expect } = require('chai');

describe('Curry', () => {
  const curry = require('./index.js');

  it('should exist', () => {
    const curry = require('./index.js');

    expect(curry).to.exist;
  });

  it('should be a function', () => {
    expect(curry).to.be.a('function');
  });

  it('should accept one argument (fn)', () => {
    expect(curry).to.have.lengthOf(1);
  });

  it('should return a function', () => {
    const curried = curry(() => {});

    expect(curried).to.be.a('function');
  });

  it('should return result when all arguments are applied', () => {
    const curried = curry((a, b) => a + b);

    expect(curried(1, 1)).to.equal(2);
  });

  it('should return n functions then result', () => {
    const curried = curry((a, b, c, d, e) => a + b + c + d + e);

    const result = curried(1)(1)(1)(1)(1);

    expect(result).to.equal(5);
  });

  it('should return result when arguments are applied irregularly', () => {
    const curried = curry((a, b, c, d, e) => a + b + c + d + e);

    const result = curried(1, 1, 1)(1)(1);

    expect(result).to.equal(5);
  });
});
