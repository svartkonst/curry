const curry = (fn) => {
  return function collect(...args) {
    return args.length === fn.length
      ? fn(...args)
      : collect.bind(null, ...args)
  }
};

module.exports = curry;
